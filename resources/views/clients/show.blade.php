<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	</head>
    <body>
    	<h1>{{ $title }}</h1>
        <table class="table">
		  <thead>
		    <tr>
		      <th>id</th>
		      <th>Name</th>
		      <th>Phone Number</th>
		      <th>Email</th>
		      <th></th>
		    </tr>
		  </thead>
		  <tbody>
	  		<tr>
	  			<td>{{ $client->id }}</td>
	  			<td>{{ $client->name }}</td>
	  			<td>{{ $client->phone_number }}</td>
	  			<td>{{ $client->email }}</td>
		  		<td><a href="/clients/{{ $client->id }}/edit">Edit</a></td>
	  		</tr>
		  </tbody>
		</table>
		<p>
			<a href="/clients">Go Back</a>
		</p>
    </body>
</html>