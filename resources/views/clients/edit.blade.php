<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	</head>
    <body>
    	<h1>{{ $title }}</h1>
    	<div class="container">
			{!! Form::open(['url' => '/clients/' . $client->id , 'method' => 'put']) !!}
			<form>
			  <div class="form-group row">
			    <label for="clientId" class="col-sm-4 col-form-label">Client id</label>
			    <div class="col-sm-8">
			    	<input type="text" class="form-control" name="clientId" id="clientId" value="{{ $client->id }}" readonly>
				</div>
			  </div>
			  <div class="form-group row">
			    <label for="clientName" class="col-sm-4 col-form-label">Client Name</label>
			    <div class="col-sm-8">
			    	<input type="text" class="form-control"  name="clientName" id="clientName" value="{{ $client->name }}">
			    </div>
			  </div>
			  <div class="form-group row">
			    <label for="clientPhoneNumber" class="col-sm-4 col-form-label">Client Phone Number</label>
			    <div class="col-sm-8">
			    	<input type="text" class="form-control" name="clientPhoneNumber" id="clientPhoneNumber" value="{{ $client->phone_number }}">
			    </div>
			  </div>
			  <div class="form-group row">
				<label for="clientEmail1" class="col-sm-4 col-form-label">Email address</label>
			    <div class="col-sm-8">
			    	<input type="email" class="form-control" name="clientEmail" id="clientEmail1" value="{{ $client->email }}">
			    </div>
			  </div>
			  <div class="form-group row">
      			<div class="col-sm-10">
			  		<button type="submit" class="btn btn-primary">Submit</button>
			  	</div>
			  	@if (session('message'))
				    <div class="alert alert-success">
				        {{ session('message') }}
				    </div>
				@endif
			  </div>
			</form>
			{!! Form::close() !!}
		</div>
    </body>
</html>