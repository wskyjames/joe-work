<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::all();

        return view ('clients/index', [
            'clients' => $clients, 
            'title' => 'Clients'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('clients/create', [
            // 'client' => $client, 
            'title' => 'Create New Client'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = new Client();

        $client->name = $request -> input('clientName');
        $client->phone_number = $request -> input('clientPhoneNumber');
        $client->email = $request -> input('clientEmail');

        $client->save();

        return redirect()->action('ClientController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {        
        return view ('clients/show', [
            'client' => $client, 
            'title' => 'Client'
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view ('clients/edit', [
            'client' => $client, 
            'title' => 'Edit Client Details'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $client->id = $request -> input('clientId');
        $client->name = $request -> input('clientName');
        $client->phone_number = $request -> input('clientPhoneNumber');
        $client->email = $request -> input('clientEmail');

        $client->save();

        return redirect()->back()->with('message','We did that thing!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}