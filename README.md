# Joe Task
  - Install Laravel
  
  - Create migrations for Clients [id, name, phone_number]
  
  - Create migrations for Addresses [id, client_id, postcode]
  
  - Seed data for both clients and addresses
  
  - Create models for clients and addresses
  
  - Install Javascript datatables for the front-end
  
  - Create a screen for viewing you client data in a datatable
  

